/*Create an activity.js file on where to write and save the solution for the activity.
Find users with letter “s” in their first name or “d” in their last name.
- Use the $or operator.
- Show only the firstName and lastName fields and hide the _id field.
Find users who are from the HR department and their age is greater than or equal to 70.
- Use the $and operator
Find users with the letter “e” in their first name and has an age of less than or equal to 30.
- Use the $and, $regex and $lte operators
Create a git repository named S29.
Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
Add the link in Boodle.*/


db.users.find({$or:[{firstName:"s"},{lastName:"d"}]},

	{
		firstName:1,
		lastName:1,
		_id:0
	}

	);


db.users.find({$and:[{department:"HR"},{age:{$gte:70}}]});


db.users.find({$and:[{firstName:{$regex:'e'}},{age:{$lte:30}}]});